resource "azurerm_resource_group" "myterraformgroup" {
  name = "${var.rg_name}"
  location = "${var.location}"
}


resource "azurerm_virtual_network" "myterraformnetwork" {
  name = "${var.avn}"
  address_space = "${var.address}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
}


resource "azurerm_public_ip" "myterraformpublicip" {
  name = "${var.api}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  public_ip_address_allocation = "dynamic"
}


resource "azurerm_subnet" "myterraformsubnet" {
  name = "${var.as}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
  address_prefix = "10.0.2.0/24"
}

resource "azurerm_network_interface" "myterraformnic" {

  name = "${var.ani}"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  ip_configuration {
    name = "${var.ip_conf}"
    subnet_id = "${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id = "${azurerm_public_ip.myterraformpublicip.id}"
  }
}





resource "azurerm_public_ip" "myterraformpublicip2" {
  name = "myPublicIP2"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  public_ip_address_allocation = "dynamic"
}
resource "azurerm_network_interface" "myterraformnic2" {

  name = "myNIC2"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  ip_configuration {
    name = "myNicConfiguration2"
    subnet_id = "${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id = "${azurerm_public_ip.myterraformpublicip2.id}"
  }
}
























resource "azurerm_network_security_group" "myterraformnsg" {
  name = "myNetworkSecurityGroup"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  security_rule {

    name                       = "SSH1"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
    }
  security_rule {
    name                       = "SSH2"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
 }
}




resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = "${azurerm_resource_group.myterraformgroup.name}"
  }

  byte_length = 8
}
resource "azurerm_storage_account" "mystorageaccount" {
  name = "diag${random_id.randomId.hex}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  location = "${var.location}"
  account_replication_type = "LRS"
  account_tier = "Standard"


}




resource "azurerm_virtual_machine" "myterraformvm" {
  name = "myVM"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_interface_ids = ["${azurerm_network_interface.myterraformnic.id}"]
  vm_size = "Standard_DS1_v2"

  storage_os_disk {
    name = "myOsDisk"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "16.04.0-LTS"
    version ="latest"
  }

  os_profile {
    computer_name = "myVM"
    admin_username = "stage"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path ="/home/stage/.ssh/authorized_keys"
      key_data ="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnYPSLgUS03yPW8P1nv9vH9wrrYRHVgzl6u2XriVyV7Vh4Fzu4GTRlIQtIQhzuX0hKF+wtzCvIWO61g7sWBIDXFPkbZm5rrKl/7XpM/wDRKcvjHHVBglZ/4ztSmXKQQ/B58FL7G4MFzmoj2clokJSsc+oos71haGIxXRfThHvCylbSiVw9+hfLIEG8c3YS5RFaMHs9aYU41Jyz+LMiNHG184hR2npfncu38ZZyJ1lrzGf38YFaTK9VVBFUN5RcCFIJGkmPZpAqDfEtffxP/zJ0/RknB2/hZ/fnRh1heN7iRTOUdLo5/vHfHsWJyucY/VcRYwOvfRHYpoOjfuyoUi2h stage@CentOs.formation"
    }
  }

  boot_diagnostics {
    enabled = "true"
    storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
  }
}







resource "azurerm_virtual_machine" "myterraformvm2" {
  name = "myVM2"
  location = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_interface_ids = ["${azurerm_network_interface.myterraformnic2.id}"]
  vm_size = "Standard_DS1_v2"

  storage_os_disk {
    name = "myOsDisk2"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "16.04.0-LTS"
    version ="latest"
  }

  os_profile {
    computer_name = "myVM2"
    admin_username = "stage"
    admin_password = "adilchemlal231086!"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  boot_diagnostics {
    enabled = "true"
    storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
  }

}

